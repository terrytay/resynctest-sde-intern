import 'package:flutter/material.dart';
import 'package:note_app/config.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(APP_TITLE),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.notifications)),
              Tab(icon: Icon(Icons.alarm)),
              Tab(icon: Icon(Icons.history)),
            ],
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text('Todo'),
                onTap: () {},
              ),
              ListTile(
                title: Text('Alarm'),
                onTap: () {},
              ),
              ListTile(
                title: Text('History'),
                onTap: () {},
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Icon(Icons.notifications),
            Icon(Icons.alarm),
            Icon(Icons.history),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {},
        ),
      ),
    );
  }
}
