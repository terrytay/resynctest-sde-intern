export interface Alarm {
  id: string;
  message: string;
  timestamp: number;
}
