import { Component, OnInit } from "@angular/core";
import { AlarmService } from "../alarm.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { Alarm } from "./alarm.model";
import { config } from "./alarm.config";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import * as firebase from "firebase/app";

@Component({
  selector: "app-alarm",
  templateUrl: "./alarm.component.html",
  styleUrls: ["./alarm.component.scss"],
})
export class AlarmComponent implements OnInit {
  editMode: boolean = false;
  alarmItems: Observable<any[]>;
  constructor(
    private db: AngularFirestore,
    private alarmService: AlarmService
  ) {}

  setAlarm(alarmTime, alarmDate, alarmMessage) {
    let alarm = {
      message: alarmMessage,
      timestamp:
        new Date((alarmDate + " " + alarmTime).replace("-", "/")).getTime() /
        1000,
    };
    if (alarm.timestamp <= new Date().getTime() / 1000) {
      alert("Please enter a future date/time.");
      return;
    }
    if (!alarm.message) {
      alert("Please enter a message.");
      return;
    }
    this.alarmService.addAlarm(alarm);
  }

  deleteButtonClick(alarm) {
    let alarmId = alarm.id;
    this.alarmService.deleteAlarm(alarmId);
  }

  editButtonClick() {
    this.editMode = !this.editMode;
  }

  editAlarmSubmit(alarm, message: string) {
    if (!message) {
      alert("Please enter a alarm");
      return;
    }

    alarm.message = message;
    this.alarmService.updateAlarm(alarm.id, alarm);
    this.editMode = false;
  }

  ngOnInit() {
    this.alarmItems = this.db
      .collection(config.collection_endpoint, (ref) =>
        ref.orderBy("timestamp", "asc")
      )
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            //Get document data
            const data = a.payload.doc.data() as Alarm;
            //Get document id
            const id = a.payload.doc.id;
            //Use spread operator to add the id to the document data
            return { id, ...data };
          });
        })
      );
    this.alarmService.clock.subscribe((time) => {
      this.alarmItems.subscribe((alarms) =>
        alarms.map((alarm) => {
          if (alarm.timestamp <= time.getTime() / 1000) {
            alert(alarm.message);
            this.alarmService.deleteAlarm(alarm.id);
          }
        })
      );
    });
  }
}
