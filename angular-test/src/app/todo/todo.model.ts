export interface Todo {
  id: string;
  message: string;
  timestamp: string;
}
