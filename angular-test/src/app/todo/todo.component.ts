import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { Todo } from "./todo.model";
import { config } from "./todo.config";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import * as firebase from "firebase/app";

@Component({
  selector: "app-todo",
  templateUrl: "./todo.component.html",
  styleUrls: ["./todo.component.scss"],
})
export class TodoComponent implements OnInit {
  isMessageEmpty: boolean = true;
  editMode: boolean = false;
  todoItems: Observable<any[]>;

  constructor(private db: AngularFirestore, private todoService: DataService) {}

  createButtonClick() {
    let timestamp = firebase.firestore.FieldValue.serverTimestamp();
    const todo = { message: "", timestamp };
    this.todoService.addTodo(todo);
    this.editMode = true;
  }

  deleteButtonClick(todo) {
    let todoId = todo.id;
    this.todoService.deleteTodo(todoId);
  }

  editButtonClick() {
    this.editMode = !this.editMode;
  }

  editTodoSubmit(todo, message: string) {
    if (!message) {
      alert("Please enter a todo");
      return;
    }

    todo.message = message;
    todo.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    this.todoService.updateTodo(todo.id, todo);
    this.editMode = false;
  }

  ngOnInit() {
    this.todoItems = this.db
      .collection(config.collection_endpoint, (ref) =>
        ref.orderBy("timestamp", "desc")
      )
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            //Get document data
            const data = a.payload.doc.data() as Todo;
            //Get document id
            const id = a.payload.doc.id;
            //Use spread operator to add the id to the document data
            return { id, ...data };
          });
        })
      );
  }
}
