import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

//Added by me for firebase config
import { AngularFireModule } from "@angular/fire";
import { AngularFireAnalyticsModule } from "@angular/fire/analytics";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../environments/environment";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";

import { AppRoutingModule } from "./app-routing.module";

import { AppComponent } from "./app.component";
import { SideBarComponent } from "./side-bar/side-bar.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TodoComponent } from "./todo/todo.component";
import { AlarmComponent } from "./alarm/alarm.component";
import { HistoryComponent } from "./history/history.component";
import { HomeComponent } from "./home/home.component";

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    TodoComponent,
    AlarmComponent,
    HistoryComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
