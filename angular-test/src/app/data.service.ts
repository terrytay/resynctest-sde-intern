import { getUrlScheme } from "@angular/compiler";
import { Injectable } from "@angular/core";
import { config } from "./todo/todo.config";
import { Todo } from "./todo/todo.model";
import {
  AngularFirestoreDocument,
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class DataService {
  todos: AngularFirestoreCollection<Todo>;
  private todoDoc: AngularFirestoreDocument<Todo>;
  todoLength: number;
  constructor(private db: AngularFirestore) {
    this.todos = db.collection<Todo>(config.collection_endpoint);
  }

  addTodo(todo) {
    this.todos.add(todo);
  }

  updateTodo(id, update) {
    this.todoDoc = this.db.doc<Todo>(`${config.collection_endpoint}/${id}`);
    this.todoDoc.update(update);
  }

  deleteTodo(id) {
    this.todoDoc = this.db.doc<Todo>(`${config.collection_endpoint}/${id}`);
    this.todoDoc.delete();
  }

  getQuantityTodo() {
    this.db
      .collection("todos")
      .valueChanges()
      .subscribe((result) => {
        this.todoLength = result.length;
      });
  }
}
