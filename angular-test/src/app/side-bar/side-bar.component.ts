import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-side-bar",
  templateUrl: "./side-bar.component.html",
  styleUrls: ["./side-bar.component.scss"],
})
export class SideBarComponent implements OnInit {
  constructor(private router: Router) {}
  sidebarItems: SideBar[] = [
    { id: "home", pageLink: "", pageTitle: "Home", icon: "fas fa-home" },
    { id: "todo", pageLink: "todo", pageTitle: "Todo", icon: "fas fa-bell" },
    {
      id: "alarm",
      pageLink: "alarm",
      pageTitle: "Alarm",
      icon: "fas fa-clock",
    },
    {
      id: "history",
      pageLink: "history",
      pageTitle: "History",
      icon: "fas fa-history",
    },
  ];

  expandSidebar = true;
  showOverlay = false;
  selectedItemIndex = "";
  selectedSubItemIndex = "";
  subMenuExpand = "";

  ngOnInit() {}

  sidebarClicked(item: SideBar) {
    this.router.navigateByUrl(item.pageLink);
  }
}

export interface SideBar {
  id: string;
  parentId?: string;
  pageLink: string;
  pageTitle: string;
  icon: string;
}
