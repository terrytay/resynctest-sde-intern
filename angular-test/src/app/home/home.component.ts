import { Component, DoCheck, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { DataService } from "../data.service";
import { AlarmService } from "../alarm.service";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit, DoCheck {
  constructor(private data: DataService, private alarm: AlarmService) {}
  tiles: TilesType[];
  ngOnInit() {
    this.data.getQuantityTodo();
    this.alarm.getQuantityAlarm();
  }
  ngDoCheck() {
    this.tiles = [
      { title: "Todo", count: this.data.todoLength },
      { title: "Alarm", count: this.alarm.alarmLength },
      { title: "History", count: 0 },
    ];
  }
}

interface TilesType {
  title: string;
  count: number;
}
