import { getUrlScheme } from "@angular/compiler";
import { Injectable } from "@angular/core";
import { Observable, interval } from "rxjs";
import { map } from "rxjs/operators";
import { config } from "./alarm/alarm.config";
import { Alarm } from "./alarm/alarm.model";
import {
  AngularFirestoreDocument,
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class AlarmService {
  alarms: AngularFirestoreCollection<Alarm>;
  private alarmDoc: AngularFirestoreDocument<Alarm>;
  alarmLength: number;
  clock: Observable<Date>;

  constructor(private db: AngularFirestore) {
    this.alarms = db.collection<Alarm>(config.collection_endpoint);

    this.clock = interval(1000).pipe(map(() => new Date()));
  }

  addAlarm(alarm) {
    this.alarms.add(alarm);
  }

  updateAlarm(id, update) {
    this.alarmDoc = this.db.doc<Alarm>(`${config.collection_endpoint}/${id}`);
    this.alarmDoc.update(update);
  }

  deleteAlarm(id) {
    this.alarmDoc = this.db.doc<Alarm>(`${config.collection_endpoint}/${id}`);
    this.alarmDoc.delete();
  }

  getQuantityAlarm() {
    this.db
      .collection("alarms")
      .valueChanges()
      .subscribe((result) => {
        this.alarmLength = result.length;
      });
  }
}
