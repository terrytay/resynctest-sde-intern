// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDcyt8kdVJhpNVjXy1y2HwggiXVdSRni8k",
    authDomain: "angular-test-6d9d5.firebaseapp.com",
    databaseURL: "https://angular-test-6d9d5.firebaseio.com",
    projectId: "angular-test-6d9d5",
    storageBucket: "angular-test-6d9d5.appspot.com",
    messagingSenderId: "352723337352",
    appId: "1:352723337352:web:47a8a1b6295a6045fd1f3d",
    measurementId: "G-7ZFRP01QQZ",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
